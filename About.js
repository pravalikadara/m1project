import React from 'react'
import "./About.css"

function About() {
  return (
  <div>
    <div>
    <img src='https://trekbaron.com/wp-content/uploads/2021/06/1-collage-variety-travel-scenery-June142021-1.jpg'
    style={({
        width:1250,
        height:400,
    })} />
   </div>
    <body>
    <div id="image-side">
    <img src="https://togethertounknown.com/wp-content/uploads/2023/01/DJI_0241-min.jpg" alt="Image 1"></img>
        <img src="https://dynamic-media-cdn.tripadvisor.com/media/photo-o/13/5e/59/d4/alleppey-backwater-tour.jpg?w=1200&h=1200&s=1" alt="Image 2"></img>
        <img src="https://www.aworldtotravel.com/wp-content/uploads/2019/11/a-7-day-ladakh-trip-itinerary-for-first-time-visitors-a-world-to-travel-1.jpg" alt="Image 2"></img>
        <img src=" https://www.fabhotels.com/blog/wp-content/uploads/2019/07/Things-to-Do-in-Manali_1400-1.jpg"alt="Image 2"></img>
        <img src=" https://lp-cms-production.imgix.net/news/2019/06/taj-mahal.jpg?auto=format&fit=crop&q=40&sharp=10&vib=20&ixlib=react-8.6.4"alt="Image 2"></img>
    </div>

    <div id="text-side">
       

        <div class="theory">
            <h4>GOA</h4>
            <p>North Goa offers sprightly beaches like Baga, Calangute, Anjuna and Arambol, that necklace the northern coastline with tons of shacks buzzing with life, serving lip-smacking delicacies from the Konkan cuisine with cocktails and mocktails to go along; Goa's beaches make your holiday experience complete.
            When it comes to tourism in India, the name Goa is a common occurrence. Mainly known for its beaches, Goa can be the perfect destination for a short holiday. Its sandy beaches and sunny weather attract people from across the globe. Moreover, it has multiple options for entertainment, like parties and nightlife, which also add to an exquisite holiday.</p>
        </div>

        <div class="theory">
            <h3>Allepy</h3>
            <p>Renowned for its boat races, beaches, marine products and coir industry, the singularity of this land is the region called Kuttanad. A land of lush paddy fields referred to as the 'Rice Bowl of Kerala', it is one of the few places in the world where farming is done below sea level. This once prosperous trading and fishing centre is nowadays a world renowned backwater tourist destination.While houseboat tours and beach visits are the two main pastimes, Alleppey also has numerous intriguing and historic sights to explore, including Vembanad Lake, Marari Beach, Alleppey Beach, Kuttanad Backwaters, Karumadi, and Pathiramanal.
</p>
        </div>
        <div class="theory">
            <h3>Ladakh</h3>
            <p>Ladakh is the highest livable region with a settlement. It is located in the northern part of Jammu and Kashmir. It is an adventure and spiritual destination wrapped into one, which has been attracting tourists with charming Buddhism culture since many years now.Ladakh is administered by the Indian government as a union territory. It is bordered by the Tibet Autonomous Region to the east, the Indian state of Himachal Pradesh to the south, both the Indian administered union territory of Jammu and Kashmir and the Pakistan to the west, and the Karakoram Pass in the far north.

</p>
        </div>
        <div class="theory">
            <h3>Manali</h3>
            <p>Besides natural charm and unparalleled beauty, Manali is known for its unlimited adventure opportunities, the famous Hadimba Temple, the scenic Rohtang Pass, the snow-laden Solang Valley and its delightful culinary scene.This list of Manali tourist places will tell you one thing – the place offers something for everyone. Enjoy the sight of misty peaks, test your endurance by engaging in a session of trekking or simply relax in the coziness of small-town hospitality – the choice is yours. So start making plans and find your calling.</p>
        </div>
        
        <div class="theory">
            <h3>The TajMahal</h3>
            <p>Tourists who spend more than three hours at the Unesco World Heritage site will now be fined, while visitors who don’t show up at the time slot specified on their ticket will not be granted access to the complex. The new restrictions have been brought in to make sure visitor numbers, which can reach as high as 50,000 per day, remain sustainable.While travellers may be slightly irked by the new limitations on their viewing and entry time, it does grant visitors more opportunity to check out some of Agra’s other star attractions, including Agra Fort, one of the finest Mughal forts in India, and Mehtab Bagh, a flower-filled park where visitors can enjoy cross-river views of the dazzling Taj for as long as they please.</p>
        </div>
      

    </div>



    </body>
  </div>
  



  )
}

export default About