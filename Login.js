import React, { useState } from 'react';
import axios from 'axios'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter,Form,Input,Label,FormGroup, Accordion } from 'reactstrap';
import Account from './Account';


function Login(args) {
 const [modal, setModal] = useState(false);
 const [formdata,setFormdata] = useState({
   email:'',
   password:''
 })


 const handleInput = (e) =>{
   const {name,value} =e.target
  
   setFormdata({
     ...formdata,
     [name]:value
       })
     
 }
 console.log(formdata)




 const toggle = (e) => {
   e.preventDefault();
   setModal(!modal)
 };






 const handleSubmit = async (e) =>{
   e.preventDefault();
   try{
     let response = await axios.get(`http://localhost:4001/login/${formdata.email}/${formdata.password}`)
     console.log(response)


   }catch (err){
     throw err
   }
   alert("Succesfully login")
  }




 return (
   <div>
     <Button color="danger" onClick={toggle}>
       login
     </Button>
     <Modal isOpen={modal} toggle={toggle} {...args}>
       <ModalHeader toggle={toggle}>Modal title</ModalHeader>
       <ModalBody>
       <>
  <Form onSubmit={handleSubmit}>
   <FormGroup floating>
     <Input
       id="exampleEmail"
       name="email"
       placeholder="Email"
       type="email"
       value = {formdata.email}
       onChange={handleInput} 
       required    


     />
     <Label for="exampleEmail">
       Email
     </Label>
   </FormGroup>
    
   <FormGroup floating>
     <Input
       id="examplePassword"
       name="password"
       placeholder="Password"
       type="password"
       value= {formdata.password}
       onChange={handleInput}
       required
     
     />
     <Label for="examplePassword">
       Password
     </Label>
   </FormGroup>
 
   <Button type='submit'>
   submit
   </Button>
 </Form>
</>
        </ModalBody>
       <ModalFooter>
         
         <Button color="secondary" onClick={toggle}>
           Cancel
         </Button>
       </ModalFooter>
     </Modal>
   </div>
 );
}


export default Login;