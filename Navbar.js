import React from 'react'
import {button,form} from 'bootstrap'
import Login from './Login'
import RegistrationForm from './Register'
function Navbar() {
  return (
    <div>
      <nav class="navbar navbar-expand-lg bg-light" data-bs-theme="light">
  <div class="container-fluid">
   <a class="navbar-brand" href="#"><img src='https://img.freepik.com/free-vector/detailed-travel-logo_23-2148627268.jpg'style={{width:150 ,height:100}}></img></a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarColor03">
      <ul class="navbar-nav me-auto">
        <li class="nav-item">
          <a class="nav-link active" href="/">Home
            <span class="visually-hidden">(current)</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/About">About</a>
          {/* <a className='nav-link' href='/About' >About</a> */}
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/Service">Service</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/Contacts">Contacts</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Dropdown</a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="#">About</a>
            <a class="dropdown-item" href="/About">Service</a>
            <a class="dropdown-item" href="#">FOUNDERS</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">Separated link</a>
          </div>
        </li>
      </ul>
      <form class="d-flex">
        <input class="form-control me-sm-2" type="search" placeholder="Search"></input>
        <button class="btn btn-secondary my-2 my-sm-0" type="submit" fdprocessedid="kvod29">Search</button>
      </form>
    </div>
  </div>
  
  <Login/>
  <RegistrationForm/>
  
</nav>
    </div>
  )
}

export default Navbar