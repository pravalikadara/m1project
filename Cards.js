function Cards(props) {
    const toggle = (e) => {
      e.preventDefault();
    }  
    return (
      <div className='col-12 col-sm-4 col-md-3 col-lg-3'>
             <img src={props.data.image} className='w-100 h-200 rounded-5 object-fit-cover' style={{height:'200px'}} />
             <div className='card-body'>
              <p className='text-black text-center fs-5'  onClick={toggle}>{props.data.cardname}</p>
              <p className='text-bold fs-4 text-center text-black'>{props.data.cardtitle}</p>
              {/* <p className='text-center text-white'>{props.data.Story}</p> */}
              </div>      
      </div>
    )
  }
  export default Cards;